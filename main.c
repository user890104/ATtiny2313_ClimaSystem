#ifndef TEST
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "pins.h"

#include "lcd/lcd_config.h"
#include "lcd/splc780d1.h"
#include "lcd/tc1604a.h"
#include "lcd/lcd.h"
#else
#include <stdio.h>
#include <windows.h>
#endif

#include "sensor/crc8.h"
#include "sensor/ds18x20.h"

#define DELAY 1000

#ifdef TEST
void _delay_ms(duration) {
    Sleep(duration);
}

#define HEATER_ON() puts("HEATER is ON")
#define HEATER_OFF() puts("HEATER is OFF")

#define FAN_ON() puts("FAN is ON")
#define FAN_OFF() puts("FAN is OFF")
#else
#define HEATER_ON() PORTD |= _BV(PD0)
#define HEATER_OFF() PORTD &= ~_BV(PD0)

#define FAN_ON() PORTD |= _BV(PD1)
#define FAN_OFF() PORTD &= ~_BV(PD1)
#endif

uint8_t buffer[9];

int main(void) {
#ifndef TEST
    DDRD |= _BV(PD0);
    DDRD |= _BV(PD1);

    lcd_init();
#endif

    while (1) {
        int16_t d;

#ifndef TEST
        DS18X20_start_meas(DS18X20_POWER_PARASITE, 0);

        _delay_ms(750);
        lcd_clear();
        lcd_home();

        if (DS18X20_read_decicelsius_single(0x28, &d) != DS18X20_OK) {
            break;
        }
#else
        _delay_ms(750);
        // 21.7*C
        uint8_t sp[DS18X20_SP_SIZE] = {0x5B, 0x01, 0x4B, 0x46, 0x7F, 0xFF, 0x05, 0x10, 0xB5};

        if (crc8(sp, DS18X20_SP_SIZE)) {
            printf("CRC ERROR\n");
            continue;
        }

        d = DS18X20_raw_to_decicelsius(0x28, sp);
#endif

        if (d <= 250) {
            HEATER_ON();
        }
        else {
            HEATER_OFF();
        }

        if (d >= 290) {
            FAN_ON();
        }
        else {
            FAN_OFF();
        }

        char temp[7];

        if (DS18X20_format_from_decicelsius(d, temp, 7) != DS18X20_OK) {
            break;
        }

#ifndef TEST
        lcd_string(temp);
        lcd_data('C');
#else
        printf("%sC\n", temp);
#endif
    }

    return 0;
}
